set title "MLS vs olm/megolm"
set xlabel "Group size"
set ylabel "Time (ms)"
set yrange [0:*]
set log x
set log y
set grid xtics mxtics ytics mytics
# uses colours from https://iamkate.com/data/12-bit-rainbow/
plot 'mls.dat' using 1:3:5 \
       with filledcurve fc rgb "#CC99DD55" lw 0 title "",\
     '' using 1:4 w lines dt 1 lc rgb "#99DD55" lw 2 title "MLS add new member", \
     'mls.dat' using 1:8:10 \
       with filledcurve fc rgb "#CC00BBCC" lw 0 title "",\
     '' using 1:9 w lines dt 2 lc rgb "#00BBCC" lw 2 title "MLS update", \
     'mls-create.dat' using 1:3:5 \
       with filledcurve fc rgb "#CC3366BB" lw 0 title "",\
     '' using 1:4 w lines dt 4 lc rgb "#3366BB" lw 2 title "MLS create", \
     'olm.dat' using 1:3:5 \
       with filledcurve fc rgb "#CCAA3355" lw 0 title "",\
     '' using 1:4 w lines dt 3 lc rgb "#AA3355" lw 2 title "new megolm session"
