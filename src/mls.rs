use base64;
use log::{debug, info};
use matrix_sdk::{
    ruma::{
        api::client::{message::send_message_event, to_device::send_event_to_device},
        events::{
            room::encrypted::{
                EncryptedEventScheme, MegolmV1AesSha2Content, MegolmV1AesSha2ContentInit,
                RoomEncryptedEventContent,
            },
            room_key::ToDeviceRoomKeyEventContent,
            AnyMessageLikeEventContent, AnyToDeviceEventContent,
        },
        serde::Raw,
        to_device::DeviceIdOrAllDevices,
        EventEncryptionAlgorithm, OwnedDeviceId, OwnedUserId, OwnedRoomId, TransactionId,
    },
    Client,
};
use openmls::prelude::*;
use openmls_rust_crypto::OpenMlsRustCrypto;
use serde_json::json;
use std::collections::BTreeMap;
use std::iter::zip;
use std::time::Instant;

// The following two helpers were taken from https://docs.rs/openmls/latest/openmls/

// A helper to create and store credentials.
fn generate_credential_bundle(
    identity: Vec<u8>,
    credential_type: CredentialType,
    signature_algorithm: SignatureScheme,
    backend: &impl OpenMlsCryptoProvider,
) -> Result<Credential, CredentialError> {
    let credential_bundle =
        CredentialBundle::new(identity, credential_type, signature_algorithm, backend)?;
    let credential_id = credential_bundle
        .credential()
        .signature_key()
        .tls_serialize_detached()
        .expect("Error serializing signature key.");
    // Store the credential bundle into the key store so OpenMLS has access
    // to it.
    backend
        .key_store()
        .store(&credential_id, &credential_bundle)
        .expect("An unexpected error occurred.");
    Ok(credential_bundle.into_parts().0)
}

// A helper to create key package bundles.
fn generate_key_package_bundle(
    ciphersuites: &[Ciphersuite],
    credential: &Credential,
    backend: &impl OpenMlsCryptoProvider,
) -> Result<KeyPackage, KeyPackageBundleNewError> {
    // Fetch the credential bundle from the key store
    let credential_id = credential
        .signature_key()
        .tls_serialize_detached()
        .expect("Error serializing signature key.");
    let credential_bundle = backend
        .key_store()
        .read(&credential_id)
        .expect("An unexpected error occurred.");

    // Create the key package bundle
    let key_package_bundle =
        KeyPackageBundle::new(ciphersuites, &credential_bundle, backend, vec![])?;

    // Store it in the key store
    let key_package_id = key_package_bundle
        .key_package()
        .hash_ref(backend.crypto())
        .expect("Could not hash KeyPackage.");
    backend
        .key_store()
        .store(key_package_id.value(), &key_package_bundle)
        .expect("An unexpected error occurred.");
    Ok(key_package_bundle.key_package().clone())
}

pub async fn run_test_mls(
    mut client: &Client,
    devices: &Vec<OwnedDeviceId>,
    room_id: &OwnedRoomId,
) -> Vec<(usize, Vec<u128>)> {
    let mls_mime: mime::Mime = "message/mls".parse().unwrap();

    let mut data: Vec<(usize, Vec<u128>)> = Vec::new();

    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;
    let backend = &OpenMlsRustCrypto::default();

    let user_id = OwnedUserId::from(client
        .user_id()
        .expect("Internal error getting user ID"));
    let main_device_id = OwnedDeviceId::from(client
        .device_id()
        .expect("Internal error getting device ID"));

    let main_mls_credential = generate_credential_bundle(
        main_device_id.as_bytes().into(),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
        backend,
    )
    .expect("An unexpected error occurred.");
    let main_key_package =
        generate_key_package_bundle(&[ciphersuite], &main_mls_credential, backend)
            .expect("An unexpected error occurred.");

    let mut backends: Vec<OpenMlsRustCrypto> = Vec::with_capacity(devices.len());
    let mls_credentials: Vec<_> = devices
        .iter()
        .map(|device| {
            let backend = OpenMlsRustCrypto::default();
            let credential = generate_credential_bundle(
                device.as_bytes().into(),
                CredentialType::Basic,
                ciphersuite.signature_algorithm(),
                &backend,
            )
            .expect("An unexpected error occurred.");
            backends.push(backend);
            credential
        })
        .collect();
    let key_packages: Vec<_> = zip(backends.iter(), mls_credentials.iter())
        .map(|(backend, credential)| {
            generate_key_package_bundle(&[ciphersuite], &credential, backend)
                .expect("An unexpected error occurred.")
        })
        .collect();

    let mut mls_group = MlsGroup::new(
        backend,
        &MlsGroupConfig::default(),
        GroupId::from_slice(room_id.as_bytes()),
        main_key_package
            .hash_ref(backend.crypto())
            .expect("Could not hash KeyPackage.")
            .as_slice(),
    )
    .expect("An unexpected error occurred.");

    debug!("Sending messages");
    let mut multiplier = 1;
    let mut last_count = 0;
    'outer: loop {
        for i in 2..=10 {
            let count = i * multiplier - 1;
            if count > devices.len() {
                break 'outer;
            }
            // add all but the last device to the group (the last new device
            // will be included in our timing)
            if count > last_count + 1 {
                let added_key_packages: Vec<_> = (&key_packages)[last_count..(count - 1)]
                    .iter()
                    .map(|kp| kp.clone())
                    .collect();
                let (_commit_mls_message_out, welcome) = mls_group
                    .add_members(backend, &added_key_packages)
                    .expect("Could not add members.");
                mls_group
                    .merge_pending_commit()
                    .expect("error merging pending commit");
                let ratchet_tree = mls_group.export_ratchet_tree();
                let mut group_from_welcome = MlsGroup::new_from_welcome(
                    &backends[count - 2],
                    &MlsGroupConfig::default(),
                    welcome.clone(),
                    Some(ratchet_tree.clone()),
                )
                .expect("Error joining group from Welcome");
                let (update_mls_message_out, _) = group_from_welcome
                    .self_update(&backends[count - 2], None)
                    .expect("error updating");

                let update_mls_message_in = MlsMessageIn::try_from_bytes(
                    &update_mls_message_out
                        .to_bytes()
                        .expect("error serializing"),
                )
                .expect("error deserializing");

                let parsed = mls_group
                    .parse_message(update_mls_message_in, backend)
                    .expect("error parsing");
                let processed = mls_group
                    .process_unverified_message(parsed, None, backend)
                    .expect("error processing message");

                match processed {
                    ProcessedMessage::StagedCommitMessage(commit) => mls_group
                        .merge_staged_commit(*commit)
                        .expect("error committing"),
                    _ => panic!("message spontaneously mutated to an unexpected type"),
                }
            }

            let add_member_start = Instant::now();
            let add_member_time: u128;

            {
                let (commit_mls_message_out, welcome) = mls_group
                    .add_members(backend, &[key_packages[count - 1].clone()])
                    .expect("Could not add member.");
                mls_group
                    .merge_pending_commit()
                    .expect("error merging pending commit");

                let commit_msg = base64::encode_config(
                    commit_mls_message_out
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );

                let welcome_msg = base64::encode_config(
                    welcome
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );

                let ratchet_tree_msg = base64::encode_config(
                    RatchetTreeExtension::new(mls_group.export_ratchet_tree())
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );

                if ratchet_tree_msg.len() > 65000 {
                    let response = client.media().upload(&mls_mime, ratchet_tree_msg.as_bytes()).await.expect("failed to upload");
                    client
                        .send(
                            send_event_to_device::v3::Request::new_raw(
                                "m.room.encrypted",
                                &TransactionId::new(),
                                BTreeMap::from([(
                                    user_id.clone(),
                                    BTreeMap::from([(
                                        DeviceIdOrAllDevices::DeviceId(devices[count - 1].clone()),
                                        Raw::new(&AnyToDeviceEventContent::RoomKey(
                                            ToDeviceRoomKeyEventContent::new(
                                                EventEncryptionAlgorithm::from("mls"),
                                                room_id.clone(),
                                                response.content_uri.to_string(),
                                                welcome_msg,
                                            ),
                                        ))
                                            .expect("Internal JSON error"),
                                    )]),
                                )]),
                            ),
                            None,
                        )
                        .await
                        .expect("Failed to send to-device");
                } else {
                    client
                        .send(
                            send_event_to_device::v3::Request::new_raw(
                                "m.room.encrypted",
                                &TransactionId::new(),
                                BTreeMap::from([(
                                    user_id.clone(),
                                    BTreeMap::from([(
                                        DeviceIdOrAllDevices::DeviceId(devices[count - 1].clone()),
                                        Raw::new(&AnyToDeviceEventContent::RoomKey(
                                            ToDeviceRoomKeyEventContent::new(
                                                EventEncryptionAlgorithm::from("mls"),
                                                room_id.clone(),
                                                ratchet_tree_msg,
                                                welcome_msg,
                                            ),
                                        ))
                                            .expect("Internal JSON error"),
                                    )]),
                                )]),
                            ),
                            None,
                        )
                        .await
                        .expect("Failed to send to-device");
                }

                if commit_msg.len() > 65000 {
                    let response = client.media().upload(&mls_mime, commit_msg.as_bytes()).await.expect("failed to upload");
                    client
                        .send(
                            send_message_event::v3::Request::new(
                                room_id,
                                &TransactionId::new(),
                                &AnyMessageLikeEventContent::RoomEncrypted(
                                    RoomEncryptedEventContent::new(
                                        EncryptedEventScheme::MegolmV1AesSha2(
                                            MegolmV1AesSha2Content::from(MegolmV1AesSha2ContentInit {
                                                ciphertext: response.content_uri.to_string(),
                                                sender_key: "".to_string(),
                                                device_id: main_device_id.clone(),
                                                session_id: "".to_string(),
                                            }),
                                        ),
                                        None,
                                    ),
                                ),
                            )
                                .unwrap(),
                            None,
                        )
                        .await
                        .expect("Failed to send commit event");
                } else {
                    client
                        .send(
                            send_message_event::v3::Request::new(
                                room_id,
                                &TransactionId::new(),
                                &AnyMessageLikeEventContent::RoomEncrypted(
                                    RoomEncryptedEventContent::new(
                                        EncryptedEventScheme::MegolmV1AesSha2(
                                            MegolmV1AesSha2Content::from(MegolmV1AesSha2ContentInit {
                                                ciphertext: commit_msg,
                                                sender_key: "".to_string(),
                                                device_id: main_device_id.clone(),
                                                session_id: "".to_string(),
                                            }),
                                        ),
                                        None,
                                    ),
                                ),
                            )
                                .unwrap(),
                            None,
                        )
                        .await
                        .expect("Failed to send commit event");
                }

                let msg = json!({
                    "room_id": room_id,
                    "type": "m.room.message",
                    "content": {
                        "body": "Hello",
                        "msgtype": "m.text",
                    },
                });
                let application_mls_message_out = mls_group
                    .create_message(backend, &serde_json::to_vec(&msg).unwrap())
                    .expect("Could not encrypt");
                let application_msg = base64::encode_config(
                    application_mls_message_out
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );
                client
                    .send(
                        send_message_event::v3::Request::new(
                            room_id,
                            &TransactionId::new(),
                            &AnyMessageLikeEventContent::RoomEncrypted(
                                RoomEncryptedEventContent::new(
                                    EncryptedEventScheme::MegolmV1AesSha2(
                                        MegolmV1AesSha2Content::from(MegolmV1AesSha2ContentInit {
                                            ciphertext: application_msg,
                                            sender_key: "".to_string(),
                                            device_id: main_device_id.clone(),
                                            session_id: "".to_string(),
                                        }),
                                    ),
                                    None,
                                ),
                            ),
                        )
                        .unwrap(),
                        None,
                    )
                    .await
                    .expect("Failed to send room event");

                add_member_time = add_member_start.elapsed().as_millis();

                // now the new member needs to send a commit so that there are no blanks
                let mut group_from_welcome = MlsGroup::new_from_welcome(
                    &backends[count - 1],
                    &MlsGroupConfig::default(),
                    welcome,
                    // The public tree is need and transferred out of band.
                    // It is also possible to use the [`RatchetTreeExtension`]
                    Some(mls_group.export_ratchet_tree()),
                )
                .expect("Error joining group from Welcome");
                let (update_mls_message_out, _) = group_from_welcome
                    .self_update(&backends[count - 1], None)
                    .expect("error updating");
                let parsed = mls_group
                    .parse_message(
                        MlsMessageIn::try_from_bytes(
                            &update_mls_message_out
                                .to_bytes()
                                .expect("error serializing"),
                        )
                        .expect("error deserializing"),
                        backend,
                    )
                    .expect("error parsing");
                let processed = mls_group
                    .process_unverified_message(parsed, None, backend)
                    .expect("error processing message");

                match processed {
                    ProcessedMessage::StagedCommitMessage(commit) => mls_group
                        .merge_staged_commit(*commit)
                        .expect("error committing"),
                    _ => panic!("message spontaneously mutated to an unexpected type"),
                }
            }

            let update_tree_start = Instant::now();

            {
                let (update_mls_message_out, _) = mls_group
                    .self_update(backend, None)
                    .expect("error updating");
                mls_group
                    .merge_pending_commit()
                    .expect("error merging pending commit");

                let update_msg = base64::encode_config(
                    update_mls_message_out
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );

                if update_msg.len() > 65000 {
                    let response = client.media().upload(&mls_mime, update_msg.as_bytes()).await.expect("failed to upload");
                    client
                        .send(
                            send_message_event::v3::Request::new(
                                room_id,
                                &TransactionId::new(),
                                &AnyMessageLikeEventContent::RoomEncrypted(
                                    RoomEncryptedEventContent::new(
                                        EncryptedEventScheme::MegolmV1AesSha2(
                                            MegolmV1AesSha2Content::from(MegolmV1AesSha2ContentInit {
                                                ciphertext: response.content_uri.to_string(),
                                                sender_key: "".to_string(),
                                                device_id: main_device_id.clone(),
                                                session_id: "".to_string(),
                                            }),
                                        ),
                                        None,
                                    ),
                                ),
                            )
                                .unwrap(),
                            None,
                        )
                        .await
                        .expect("Failed to send commit event");
                } else {
                    client
                        .send(
                            send_message_event::v3::Request::new(
                                room_id,
                                &TransactionId::new(),
                                &AnyMessageLikeEventContent::RoomEncrypted(
                                    RoomEncryptedEventContent::new(
                                        EncryptedEventScheme::MegolmV1AesSha2(
                                            MegolmV1AesSha2Content::from(MegolmV1AesSha2ContentInit {
                                                ciphertext: update_msg,
                                                sender_key: "".to_string(),
                                                device_id: main_device_id.clone(),
                                                session_id: "".to_string(),
                                            }),
                                        ),
                                        None,
                                    ),
                                ),
                            )
                                .unwrap(),
                            None,
                        )
                        .await
                        .expect("Failed to send commit event");
                }

                let msg = json!({
                    "room_id": room_id,
                    "type": "m.room.message",
                    "content": {
                        "body": "Hello",
                        "msgtype": "m.text",
                    },
                });
                let application_mls_message_out = mls_group
                    .create_message(backend, &serde_json::to_vec(&msg).unwrap())
                    .expect("Could not encrypt");
                let application_msg = base64::encode_config(
                    application_mls_message_out
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );
                client
                    .send(
                        send_message_event::v3::Request::new(
                            room_id,
                            &TransactionId::new(),
                            &AnyMessageLikeEventContent::RoomEncrypted(
                                RoomEncryptedEventContent::new(
                                    EncryptedEventScheme::MegolmV1AesSha2(
                                        MegolmV1AesSha2Content::from(MegolmV1AesSha2ContentInit {
                                            ciphertext: application_msg,
                                            sender_key: "".to_string(),
                                            device_id: main_device_id.clone(),
                                            session_id: "".to_string(),
                                        }),
                                    ),
                                    None,
                                ),
                            ),
                        )
                        .unwrap(),
                        None,
                    )
                    .await
                    .expect("Failed to send room event");
            }

            let update_tree_time = update_tree_start.elapsed().as_millis();
            data.push((count + 1, vec![add_member_time, update_tree_time]));

            assert_eq!(mls_group.members().len(), count + 1);

            last_count = count;
        }
        multiplier *= 10;
    }

    return data;
}
