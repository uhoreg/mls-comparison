use log::{debug, info};
use matrix_sdk::{
    ruma::{
        api::client::{message::send_message_event, to_device::send_event_to_device},
        events::{
            room::encrypted::{
                CiphertextInfo, EncryptedEventScheme, MegolmV1AesSha2Content,
                MegolmV1AesSha2ContentInit, OlmV1Curve25519AesSha2Content,
                RoomEncryptedEventContent, ToDeviceRoomEncryptedEventContent,
            },
            room_key::ToDeviceRoomKeyEventContent,
            AnyMessageLikeEventContent, AnyToDeviceEventContent,
        },
        serde::Raw,
        to_device::DeviceIdOrAllDevices,
        EventEncryptionAlgorithm, OwnedDeviceId, OwnedUserId, OwnedRoomId, TransactionId,
    },
    Client,
};
use serde_json::json;
use std::collections::BTreeMap;
use std::time::Instant;
use vodozemac::megolm::GroupSession;
use vodozemac::olm::{Account, Session};
use vodozemac::{Curve25519PublicKey, Ed25519PublicKey};

pub async fn run_test_olm(
    mut client: &Client,
    devices: &Vec<OwnedDeviceId>,
    room_id: &OwnedRoomId,
) -> Vec<(usize, Vec<u128>)> {
    let mut data: Vec<(usize, Vec<u128>)> = Vec::new();

    let user_id = OwnedUserId::from(client
        .user_id()
        .expect("Internal error getting user ID"));
    let main_device_id = OwnedDeviceId::from(client
        .device_id()
        .expect("Internal error getting device ID"));

    let main_olm_account = Account::new();
    let olm_accounts: Vec<Account> = devices.iter().map(|_| Account::new()).collect();
    let curve25519_keys: Vec<Curve25519PublicKey> = olm_accounts
        .iter()
        .map(|acc| acc.curve25519_key().clone())
        .collect();
    let ed25519_keys: Vec<Ed25519PublicKey> = olm_accounts
        .iter()
        .map(|acc| acc.ed25519_key().clone())
        .collect();

    let mut otks: Vec<Curve25519PublicKey> = Vec::new();

    for mut acc in olm_accounts {
        acc.generate_one_time_keys(1);
        otks.push(
            *acc.one_time_keys()
                .values()
                .next()
                .expect("Internal vodozemac error"),
        );
    }

    let mut sessions: Vec<Session> = Vec::new();

    debug!("Sending messages");
    let mut multiplier = 1;
    let mut last_count = 0;
    'outer: loop {
        for i in 2..=10 {
            let count = i * multiplier - 1;
            if count > devices.len() {
                break 'outer;
            }
            info!("device #{}", count);
            // ensure that we have olm sessions for all but the last new device
            // (the last new device will be included in our timing)
            if count >= last_count + 1 {
                for j in last_count..(count - 1) {
                    sessions.push(
                        main_olm_account.create_outbound_session(curve25519_keys[j], otks[j]),
                    );
                }
            }

            let send_message_session_time = Instant::now();
            let mut megolm_session = GroupSession::new();
            let key = megolm_session.session_key();
            let plaintext = json!({
                "content": AnyToDeviceEventContent::RoomKey(ToDeviceRoomKeyEventContent::new(
                    EventEncryptionAlgorithm::MegolmV1AesSha2,
                    room_id.clone(),
                    megolm_session.session_id(),
                    key.to_base64()
                )),
                "sender": user_id,
                "recipient": user_id,
                "recipient_keys": { "ed25519": ed25519_keys[count - 1].to_base64() },
                "keys": { "ed25519": main_olm_account.ed25519_key().to_base64() },
                "type": "m.room_key",
            });
            sessions.push(
                main_olm_account
                    .create_outbound_session(curve25519_keys[count - 1], otks[count - 1]),
            );

            assert_eq!(sessions.len(), count);

            let mut ciphertexts: BTreeMap<DeviceIdOrAllDevices, Raw<AnyToDeviceEventContent>> =
                BTreeMap::new();
            for j in 0..sessions.len() {
                let ciphertext = sessions[j].encrypt(&serde_json::to_string(&plaintext).unwrap());
                let (message_type, body) = ciphertext.to_parts();
                ciphertexts.insert(
                    DeviceIdOrAllDevices::DeviceId(devices[j].clone()),
                    Raw::new(&AnyToDeviceEventContent::RoomEncrypted(
                        ToDeviceRoomEncryptedEventContent::new(
                            EncryptedEventScheme::OlmV1Curve25519AesSha2(
                                OlmV1Curve25519AesSha2Content::new(
                                    BTreeMap::from([(
                                        curve25519_keys[j].to_base64(),
                                        CiphertextInfo::new(
                                            body,
                                            js_int::UInt::new(if message_type == 0 {
                                                0
                                            } else {
                                                1
                                            })
                                            .expect("Internal JSON error"),
                                        ),
                                    )]),
                                    main_olm_account.curve25519_key().to_base64(),
                                ),
                            ),
                        ),
                    ))
                    .expect("Internal JSON error"),
                );

                if ciphertexts.len() >= 200 {
                    client
                        .send(
                            send_event_to_device::v3::Request::new_raw(
                                "m.room.encrypted",
                                &TransactionId::new(),
                                BTreeMap::from([(user_id.clone(), ciphertexts.clone())]),
                            ),
                            None,
                        )
                        .await
                        .expect("Failed to send to-device");

                    ciphertexts.clear();
                }
            }

            if !ciphertexts.is_empty() {
                client
                    .send(
                        send_event_to_device::v3::Request::new_raw(
                            "m.room.encrypted",
                            &TransactionId::new(),
                            BTreeMap::from([(user_id.clone(), ciphertexts)]),
                        ),
                        None,
                    )
                    .await
                    .expect("Failed to send to-device");
            }

            let msg = json!({
                "room_id": room_id,
                "type": "m.room.message",
                "content": {
                    "body": "Hello",
                    "msgtype": "m.text",
                },
            });
            client
                .send(
                    send_message_event::v3::Request::new(
                        room_id,
                        &TransactionId::new(),
                        &AnyMessageLikeEventContent::RoomEncrypted(RoomEncryptedEventContent::new(
                            EncryptedEventScheme::MegolmV1AesSha2(MegolmV1AesSha2Content::from(
                                MegolmV1AesSha2ContentInit {
                                    ciphertext: megolm_session
                                        .encrypt(&serde_json::to_string(&msg).unwrap())
                                        .to_base64(),
                                    sender_key: main_olm_account.curve25519_key().to_base64(),
                                    device_id: main_device_id.clone(),
                                    session_id: megolm_session.session_id(),
                                },
                            )),
                            None,
                        )),
                    )
                    .unwrap(),
                    None,
                )
                .await
                .expect("Failed to send room event");

            data.push((
                count + 1,
                vec![send_message_session_time.elapsed().as_millis()],
            ));

            last_count = count;
        }
        multiplier *= 10;
    }

    return data;
}
