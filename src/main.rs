use clap::{ArgEnum, Parser};
use log::{debug, info};
use matrix_sdk::{
    config::SyncSettings,
    ruma::{
        api::client::{membership::leave_room, room::create_room},
        device_id, OwnedDeviceId, UserId,
    },
    Client, Session,
};
use std::collections::BTreeMap;
use std::time::Duration;

mod olm;
use crate::olm::run_test_olm;
mod mls;
use crate::mls::run_test_mls;
mod mls_create;
use crate::mls_create::run_test_mls_create;

#[derive(Parser)]
struct Opts {
    /// size of the room
    #[clap(long)]
    size: u32,

    /// type of encryption to use
    #[clap(arg_enum, long = "type")]
    e2e_type: E2eType,

    /// homeserver URL
    #[clap(long)]
    homeserver: String,

    /// user ID
    #[clap(long)]
    user: String,

    /// access token
    #[clap(long)]
    access_token: String,

    /// number of runs
    #[clap(long)]
    runs: u32,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
enum E2eType {
    Olm,
    Mls,
    MlsCreate,
}

fn get_stats(
    mut iter: impl Iterator<Item = Vec<(usize, Vec<u128>)>>,
) -> Vec<(usize, Vec<(u128, u128, u128, u128, u128)>)> {
    let mut data: BTreeMap<usize, Vec<Vec<u128>>> = BTreeMap::new();

    for items in iter {
        items
            .iter()
            .for_each(|(i, nested_items)| match data.get_mut(&i) {
                Some(vec) => {
                    nested_items
                        .iter()
                        .enumerate()
                        .for_each(|(j, item)| vec[j].push(*item));
                }
                None => {
                    data.insert(*i, nested_items.iter().map(|item| vec![*item]).collect());
                }
            });
    }

    let make_stat = |items: &Vec<u128>| -> (u128, u128, u128, u128, u128) {
        match &items[..] {
            [elem] => (*elem, *elem, *elem, *elem, *elem),
            [first, second] => (*first, *first, (first + second) / 2, *second, *second),
            [bot, middle, top] => (*bot, *bot, *middle, *top, *top),
            [bot, bot_mid, top_mid, top] => (*bot, *bot, (bot_mid + top_mid), *top, *top),
            _ => {
                let count = items.len();
                let middle = count >> 1;
                let median = match count % 2 {
                    1 => items[middle],
                    _ => (items[middle - 1] + items[middle]) / 2,
                };
                let quart_pos = count >> 2;
                let sixteenth_pos = std::cmp::max(count >> 4, 1);
                let (bot_sixteenth, bot_quart, top_quart, top_sixteenth) = match count % 4 {
                    2 | 3 => (
                        items[sixteenth_pos],
                        items[quart_pos],
                        items[count - 1 - quart_pos],
                        items[count - 1 - sixteenth_pos],
                    ),
                    _ => (
                        (items[sixteenth_pos - 1] + items[sixteenth_pos]) / 2,
                        (items[quart_pos - 1] + items[quart_pos]) / 2,
                        (items[count - 1 - quart_pos] + items[count - quart_pos]) / 2,
                        (items[count - 1 - sixteenth_pos] + items[count - sixteenth_pos]) / 2,
                    ),
                };
                (bot_sixteenth, bot_quart, median, top_quart, top_sixteenth)
            }
        }
    };

    data.iter_mut()
        .map(|(i, row)| {
            (
                *i,
                row.iter_mut()
                    .map(|items| {
                        items.sort();
                        make_stat(&items)
                    })
                    .collect(),
            )
        })
        .collect()
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();

    let args = Opts::parse();
    let client = Client::builder()
        .homeserver_url(&args.homeserver)
        .build()
        .await?;

    debug!("Logging in as {}", &args.user);

    let session = Session {
        access_token: args.access_token,
        refresh_token: None,
        user_id: UserId::parse(args.user).expect("Unable to parse user ID"),
        device_id: device_id!("ALICEDEVICE0").to_owned(),
    };

    client.restore_login(session).await?;

    let sync_resp = client
        .sync_once(SyncSettings::default().timeout(Duration::from_secs(0)))
        .await?;

    // leave all joined/invited rooms
    for room_id in sync_resp.rooms.join.keys() {
        debug!("Leaving {}", &room_id);
        client
            .send(leave_room::v3::Request::new(room_id), None)
            .await?;
    }

    for room_id in sync_resp.rooms.invite.keys() {
        debug!("Leaving {}", &room_id);
        client
            .send(leave_room::v3::Request::new(room_id), None)
            .await?;
    }

    let devices: Vec<OwnedDeviceId> = (0..(args.size - 1))
        .into_iter()
        .map(|num| OwnedDeviceId::from(format!("ALICEDEVICE{}", num + 1)))
        .collect();

    let mut runs: Vec<Vec<(usize, Vec<u128>)>> = Vec::new();

    for i in 0..args.runs {
        info!("Run #{}", i + 1);
        let create_response = client.create_room(create_room::v3::Request::new()).await?;

        runs.push(match args.e2e_type {
            E2eType::Olm => run_test_olm(&client, &devices, &create_response.room_id).await,
            E2eType::Mls => run_test_mls(&client, &devices, &create_response.room_id).await,
            E2eType::MlsCreate => run_test_mls_create(&client, &devices, &create_response.room_id).await,
        });
        client
            .send(leave_room::v3::Request::new(&create_response.room_id), None)
            .await?;
    }

    let stats = get_stats(runs.into_iter());

    stats.iter().for_each(|(i, data)| {
        println!(
            "{} {}",
            i,
            data.iter()
                .map(|(bot, bot_quart, mid, top_quart, top)| format!(
                    "{} {} {} {} {}",
                    bot, bot_quart, mid, top_quart, top
                ))
                .collect::<Vec<_>>()
                .join(" ")
        )
    });

    Ok(())
}
