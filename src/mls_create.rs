use base64;
use log::{debug, info};
use matrix_sdk::{
    ruma::{
        api::client::{message::send_message_event, to_device::send_event_to_device},
        events::{
            room::encrypted::{
                EncryptedEventScheme, MegolmV1AesSha2Content, MegolmV1AesSha2ContentInit,
                RoomEncryptedEventContent,
            },
            room_key::ToDeviceRoomKeyEventContent,
            AnyMessageLikeEventContent, AnyToDeviceEventContent,
        },
        serde::Raw,
        to_device::DeviceIdOrAllDevices,
        EventEncryptionAlgorithm, OwnedDeviceId, OwnedUserId, OwnedRoomId, TransactionId,
    },
    Client,
};
use openmls::prelude::*;
use openmls_rust_crypto::OpenMlsRustCrypto;
use serde_json::json;
use std::collections::BTreeMap;
use std::iter::zip;
use std::time::Instant;

// The following two helpers were taken from https://docs.rs/openmls/latest/openmls/

// A helper to create and store credentials.
fn generate_credential_bundle(
    identity: Vec<u8>,
    credential_type: CredentialType,
    signature_algorithm: SignatureScheme,
    backend: &impl OpenMlsCryptoProvider,
) -> Result<Credential, CredentialError> {
    let credential_bundle =
        CredentialBundle::new(identity, credential_type, signature_algorithm, backend)?;
    let credential_id = credential_bundle
        .credential()
        .signature_key()
        .tls_serialize_detached()
        .expect("Error serializing signature key.");
    // Store the credential bundle into the key store so OpenMLS has access
    // to it.
    backend
        .key_store()
        .store(&credential_id, &credential_bundle)
        .expect("An unexpected error occurred.");
    Ok(credential_bundle.into_parts().0)
}

// A helper to create key package bundles.
fn generate_key_package_bundle(
    ciphersuites: &[Ciphersuite],
    credential: &Credential,
    backend: &impl OpenMlsCryptoProvider,
) -> Result<KeyPackage, KeyPackageBundleNewError> {
    // Fetch the credential bundle from the key store
    let credential_id = credential
        .signature_key()
        .tls_serialize_detached()
        .expect("Error serializing signature key.");
    let credential_bundle = backend
        .key_store()
        .read(&credential_id)
        .expect("An unexpected error occurred.");

    // Create the key package bundle
    let key_package_bundle =
        KeyPackageBundle::new(ciphersuites, &credential_bundle, backend, vec![])?;

    // Store it in the key store
    let key_package_id = key_package_bundle
        .key_package()
        .hash_ref(backend.crypto())
        .expect("Could not hash KeyPackage.");
    backend
        .key_store()
        .store(key_package_id.value(), &key_package_bundle)
        .expect("An unexpected error occurred.");
    Ok(key_package_bundle.key_package().clone())
}

pub async fn run_test_mls_create(
    mut client: &Client,
    devices: &Vec<OwnedDeviceId>,
    room_id: &OwnedRoomId,
) -> Vec<(usize, Vec<u128>)> {
    let mls_mime: mime::Mime = "message/mls".parse().unwrap();

    let mut data: Vec<(usize, Vec<u128>)> = Vec::new();

    let ciphersuite = Ciphersuite::MLS_128_DHKEMX25519_AES128GCM_SHA256_Ed25519;
    let backend = &OpenMlsRustCrypto::default();

    let user_id = OwnedUserId::from(client
        .user_id()
        .expect("Internal error getting user ID"));
    let main_device_id = OwnedDeviceId::from(client
        .device_id()
        .expect("Internal error getting device ID"));

    let main_mls_credential = generate_credential_bundle(
        main_device_id.as_bytes().into(),
        CredentialType::Basic,
        ciphersuite.signature_algorithm(),
        backend,
    )
    .expect("An unexpected error occurred.");

    let mut backends: Vec<OpenMlsRustCrypto> = Vec::with_capacity(devices.len());
    let mls_credentials: Vec<_> = devices
        .iter()
        .map(|device| {
            let backend = OpenMlsRustCrypto::default();
            let credential = generate_credential_bundle(
                device.as_bytes().into(),
                CredentialType::Basic,
                ciphersuite.signature_algorithm(),
                &backend,
            )
            .expect("An unexpected error occurred.");
            backends.push(backend);
            credential
        })
        .collect();
    let key_packages: Vec<_> = zip(backends.iter(), mls_credentials.iter())
        .map(|(backend, credential)| {
            generate_key_package_bundle(&[ciphersuite], &credential, backend)
                .expect("An unexpected error occurred.")
        })
        .collect();

    let mut multiplier = 1;
    'outer: loop {
        for i in 2..=10 {
            let count = i * multiplier - 1;
            if count > devices.len() {
                break 'outer;
            }

            let create_start = Instant::now();

            let main_key_package =
                generate_key_package_bundle(&[ciphersuite], &main_mls_credential, backend)
                .expect("An unexpected error occurred.");
            let mut mls_group = MlsGroup::new(
                backend,
                &MlsGroupConfig::default(),
                GroupId::from_slice(room_id.as_bytes()),
                main_key_package
                    .hash_ref(backend.crypto())
                    .expect("Could not hash KeyPackage.")
                    .as_slice(),
            )
                .expect("An unexpected error occurred.");

            let added_key_packages: Vec<_> = (&key_packages)[0..count]
                .iter()
                .map(|kp| kp.clone())
                .collect();
            let (_commit_mls_message_out, welcome) = mls_group
                .add_members(backend, &added_key_packages)
                .expect("Could not add members.");
            mls_group
                .merge_pending_commit()
                .expect("error merging pending commit");

                let welcome_msg = base64::encode_config(
                    welcome
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );

                let ratchet_tree_msg = base64::encode_config(
                    RatchetTreeExtension::new(mls_group.export_ratchet_tree())
                        .tls_serialize_detached()
                        .expect("Could not serialize"),
                    base64::STANDARD_NO_PAD,
                );

            let welcome =
                if welcome_msg.len() > 65000 {
                    let welcome_response = client.media().upload(&mls_mime, welcome_msg.as_bytes()).await.expect("failed to upload");
                    welcome_response.content_uri.to_string()
                } else {
                    welcome_msg
                };

            let response = client.media().upload(&mls_mime, ratchet_tree_msg.as_bytes()).await.expect("failed to upload");

            for j in 0..(count / 200) {
            let messages = devices[(j * 200)..((j + 1) * 200)]
                    .iter()
                    .map(|device| (DeviceIdOrAllDevices::DeviceId(device.clone()),
                                        Raw::new(&AnyToDeviceEventContent::RoomKey(
                                            ToDeviceRoomKeyEventContent::new(
                                                EventEncryptionAlgorithm::from("mls"),
                                                room_id.clone(),
                                                response.content_uri.to_string(),
                                                welcome.clone(),
                                            ),
                                        ))
                                            .expect("Internal JSON error"),
                    ))
                    .collect::<BTreeMap<_, _>>();
            client
                .send(
                    send_event_to_device::v3::Request::new_raw(
                        "m.room.encrypted",
                        &TransactionId::new(),
                        BTreeMap::from([(
                            user_id.clone(),
                            messages,
                        )]),
                    ),
                    None,
                )
                .await
                .expect("Failed to send to-device");
            }
            let messages = devices[(count / 200 * 200)..count]
                    .iter()
                    .map(|device| (DeviceIdOrAllDevices::DeviceId(device.clone()),
                                        Raw::new(&AnyToDeviceEventContent::RoomKey(
                                            ToDeviceRoomKeyEventContent::new(
                                                EventEncryptionAlgorithm::from("mls"),
                                                room_id.clone(),
                                                response.content_uri.to_string(),
                                                welcome.clone(),
                                            ),
                                        ))
                                            .expect("Internal JSON error"),
                    ))
                    .collect::<BTreeMap<_, _>>();
            client
                .send(
                    send_event_to_device::v3::Request::new_raw(
                        "m.room.encrypted",
                        &TransactionId::new(),
                        BTreeMap::from([(
                            user_id.clone(),
                            messages,
                        )]),
                    ),
                    None,
                )
                .await
                .expect("Failed to send to-device");

            data.push((count + 1, vec![create_start.elapsed().as_millis()]));
        }
        multiplier *= 10;
    }

    return data;
}
