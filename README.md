Olm/Megolm and MLS comparison
=============================

Performance comparison between Olm/Megolm and MLS.

Written in rust, and requires a recent rust to be installed.

Requires an account on a Matrix homeserver, with pre-created devices.  The
device IDs must be of the form `ALICEDEVICEnnn` where `nnn` is a number,
starting from 0.  You can use the docker image
`gitlab-registry.matrix.org/uhoreg/mls-comparison/synapse`, which has a synapse
with 100,000 pre-generated devices (so you can use a `--size` parameter of up
to `100000`).  Use `@alice:hs1` as the user ID and
`syt_YWxpY2U_hUdfzUbgOnYxxtgFdlLH_0X0SZS` as the access token.

For example, you can run

```
docker run --rm -d -p 8008:8008 gitlab-registry.matrix.org/uhoreg/mls-comparison/synapse
```

to start the homeserver and listen on port 8008.

Can be run as:

```
cargo run -- --size {number} --type {olm, mls, or mls-create} --homeserver {homeserver URL} --user {user ID} --access-token {access token} --runs {number}
```

* `--size` gives the number of devices to use.
* `--access-token` must be the access token for device `ALICEDEVICE0`

The output will be written in a format suitable for the `comparison.gnuplot`
file, for generating a graph with [gnuplot](http://gnuplot.info/).  You can
redirect the output to appropriate files to generate the graphs.  The gnuplot
file expects the files to be named `x.dat` where `x` is `olm`, `mls`, or
`mls-create` depending on the `--type` argument given.

For example, with the homeserver from the above Docker image, you can run

```
cargo run -- --size 100000 --type mls --homeserver http://localhost:8008 --user @alice:hs1 --access-token syt_YWxpY2U_hUdfzUbgOnYxxtgFdlLH_0X0SZS --runs 10 > mls.dat
```

Multiple runs of the test can be performed, and the timings will be aggregated.
For each room size, the timings will be given as: the 1st 16-quantile, the 1st
quartile (25th percentile), the median, the 3rd quartile (75th percentile), and
the 15th 16-quantile.

## Tests

It runs the following tests:

- simulate devices joining a room one at a time, with a message sent in between
  each join
  - Olm/Megolm: creates a new Megolm session for each message.  Time measured
    includes:
    - creating a new megolm session
    - creating a new olm session for the new user
    - encrypting the megolm session and sending it to each user
    - encrypting a message and sending it to the room
  - MLS: (note: the speed of MLS operations can greatly depend on how many
    internal nodes have keys, and how many are blank.  The tests create a
    fairly optimistic situation.  Speed under other situations could be quite
    different.)
    - adds a device to the group.  Time measured includes:
      - add a member to the group
      - send the MLS Commit message to the room (uploading to media repository
        if too large to fit in a Matrix event)
      - send the MLS Welcome message to the added device
      - encrypting a message and sending it to the room
    - updates the root secret.  Time measured includes:
      - update the root secret
      - send the MLS Commit message to the room (uploading to media repository
        if too large to fit in a Matrix event)
      - encrypting a message and sending it to the room
- create a new room from scratch
  - Olm/Megolm: since Olm/Megolm does not distinguish between creating a new
    room and creating a new Megolm session for an existing room, this timing
    would be largely the same as the one for the Olm/Megolm test given above.
    The main difference would be that multiple Olm sessions might need to be
    created, depending on what devices the sender had previously communicated
    with.
  - MLS: creates an MLS group of the given size.  Time measured includes:
    - creating the MLS group
    - adding the members to the group in a single Add operation
    - uploading the ratchet tree to the media repository
    - sending a Welcome message to each device (uploading to media repository
      if too large to fit in a Matrix event)
- TODO: add more?
